# Motiontag Coding Challenge 

This is the repository of the motiontag coding challenge

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [OpenCSV](http://opencsv.sourceforge.net/) - Used to generate RSS Feeds

## Authors

* **Ramin Gharib** [raminqaf](https://github.com/raminqaf)
