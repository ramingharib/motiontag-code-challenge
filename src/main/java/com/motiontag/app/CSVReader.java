package com.motiontag.app;

import com.motiontag.model.Coordinate;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

class CSVReader {

    private static final String SAMPLE_CSV_FILE_PATH = "/Users/ramin/Documents/IntelliJ/CodingChallenge/src/main/resources/Coordinates.csv";

    static void ReadCSV() {

        try (Reader reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH))) {
            CsvToBeanBuilder csvToBeanBuilder = new CsvToBeanBuilder(reader);
            csvToBeanBuilder.withType(Coordinate.class);
            csvToBeanBuilder.withIgnoreLeadingWhiteSpace(true);
            CsvToBean csvToBean = csvToBeanBuilder.build();

            List<Coordinate> csvUsers = csvToBean.parse();

//            for (Coordinate coordinate : csvUsers) {
//                System.out.println("Id : " + coordinate.getId());
//                System.out.println("Lat : " + coordinate.getLat());
//                System.out.println("Long : " + coordinate.getLon());
//                System.out.println("Accuracy : " + coordinate.getAccuracy());
//                System.out.println("Time (min) : " + coordinate.getTime());
//                System.out.println("Outlier : " + coordinate.isOutlier());
//                System.out.println("==========================");
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
