package com.motiontag.model;

import com.opencsv.bean.CsvBindByName;

public class Coordinate {
    @CsvBindByName(column = "id", required = true)
    private int id;

    @CsvBindByName(column = "lat", required = true)
    private double lat;

    @CsvBindByName(column = "lon", required = true)
    private double lon;

    @CsvBindByName(column = "Accuracy", required = true)
    private int accuracy;

    @CsvBindByName(column = "Time (min)", required = true)
    private int time;

    @CsvBindByName(column = "Outlier", required = true)
    private boolean outlier;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean isOutlier() {
        return outlier;
    }

    public void setOutlier(boolean outlier) {
        this.outlier = outlier;
    }
}
